var mongoose = require("mongoose");

var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/fotos", {useNewUrlParser: true}, (err, res) => {
if (err) throw err;

console.log('Database online');
});
//mongoose.connect("mongodb://localhost/fotos", {useNewUrlParser: true}).then(() => console.log('MongoDB Connected')).catch((err) => console.log(err));

var pos_sex = ["F","M"];

var email_match = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,"Formato incorrecto"];

var pw_validator = {
                validator: 
                function(p){
                        return this.password_confirmation == p;
                },message:"La clave no coinside"
        };
var user_schema = new Schema({
	nombre:String,
	username:{type: String, required:true,maxlength:[30,"Largo no permitido"]},
	pw:{type: String, required:true,minlength:[6,"Minino no permitido"],validate:pw_validator},
	edad:{type: Number,min:[18,"Su edad debe ser mayor a 18"],max:[100,"Edad no valida"]},
	email:{type: String, required: "Email es obligatorio",match:email_match},
	sexo:{type:String,enum:{values: pos_sex,message:"Opción no valida"}},
	fecha_nac:Date
});

user_schema.virtual("password_confirmation").get(function(){
	return this.pw_confirmation;
}).set(function(pw){
	this.pw_confirmation = pw;
});

/*
 *String
 *Number
 *Date
 *Buffer
 *Boolean
 *Mixed
 *Objectid
 *Array
*/

var User = mongoose.model("user",user_schema);
var User1 = mongoose.model("Usuario",user_schema);

module.exports.User = User;
