var express = require("express");
var session = require("express-session");
var bodyParser = require("body-parser");
var User = require("./models/user").User;
var session_middleware = require("./middlewares/session");
var router_app = require("./routes_app");

var app = express();

app.use('/csspublic', express.static('./public/css'));
app.use('/css', express.static('./bootstrap/css'));
app.use('/js', express.static(__dirname + '/bootstrap/js'));

app.use(bodyParser.json()); //Peticiones application/json
app.use(bodyParser.urlencoded({extended: true}));

app.use(session({
	secret: 'qwertyuiopasdfghjkl',
	resave: true,
	saveUninitialized: true
}));

/* /app need login*/
/* / not need login*/

app.set("view engine","jade");

app.get("/",function(req,res){
	console.log(req.session);
	console.log(" ### ");
	console.log(req.session.user_id);
	res.render("indexx");
});

app.get("/singup",function (req,res){
	User.find(function (err,doc){
		console.log(doc);
	});
	res.render("singup");
});

app.get("/login",function (req,res){
        res.render("login");
});

app.post("/sessions",function(req,res){
	User.findOne({email:req.body.email,pw: req.body.pw},"",function(err,user){
		console.log(user);
		req.session.user_id = user._id;
		res.redirect("/app");
	});
});

app.post("/users",function(req,res){
	var user = new User({	
				username: req.body.username,
				email:req.body.email,
				pw: req.body.pw,
				password_confirmation: req.body.password_confirmation
			});
	user.save().then(function(us){
		res.send("SE GUARDO LOS DATOS");
	},function(err){
		if(err){
			console.log(String(err));
			res.send("No se Pudo guardar la data");
		}
	})
	/*
	console.log(user.password_confirmation);
	user.save(function(err,user,numero){
		if(err){
			console.log(String(err));
		}
		res.send("SE GUARDO LOS DATOS");
	});
	*/
	console.log("Email: "+ req.body.email);
	console.log("Clave: "+ req.body.pw);
});

app.use("/app", session_middleware)
app.use("/app", router_app);

app.listen(3000);
