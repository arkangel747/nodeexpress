var express = require("express");

var app = express();

app.set("view engine","jade");

app.get("/",function (req,res){
	//res.render("index",{Hola:"HOLA ARKANGEL"});
	res.render("form");
});

app.post("/",function(req,res){
	res.render("form_post");
});

app.get("/:var",function(req,res){
	res.render("form_post",{variable:req.params.var});
});

app.listen(3000);